## Getting started
# Info 
Application server: ```curl http://51.20.18.205 | jq```

## Application Description
For creating HTTP application, I have used Python language and FastAPI framework. 
The different metrics I got by different approaches. 
- Memory: For getting memory information, I have user /proc/meminfo data.
- Disk: For getting disk data, I have used a perfectly working solution with tons of different other metrics - node_exporter. The big plus for this solution is that an application could be extended with other metrics very easily. (Memory information is also available here)
- Uptime: I don't think it needs to be explained.

# CI/CD
## CI
All code is built into a docker container and published on Docker HUB.
The CI part contains only the "Build" step. I didn't realize code checking, code security checks, unit testing, and so on (it will take more time).
## CD
One of the tasks requirements was to implement "Zero deployment downtime". I chose blue-green
deployment for this task.
To realize that blue-green deployment is possible only with a third party balancer. I decided to choose most popular - nginx.
For code deploying, I take an ansible.
There are 2 ansible roles: provision and deploy.
Provision was used for basic provisioning (nginx, docker-compose)
The deploy role is used to run the deploy.sh script, where all deploy logic is realized

## P.S.
This is just a POC project. And I see a lot of things that could be improved.
