
from typing import Union

from fastapi import FastAPI

from .lib import disk, memory

import time

start_time = time.time()
app = FastAPI()

@app.get("/")
def read_root():
    total_disks = disk.disk()
    total_memory, available_memory = memory.memory()
    return {
        "Memory":{"Total": total_memory, "Available": available_memory},
        "Disk": total_disks,
        "Uptime": int(time.time() - start_time)
        }
