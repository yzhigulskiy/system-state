# import os
import requests

def disk():
    result = {}
    data = requests.get('http://node_exporter:9100/metrics').text.split('\n')
    
    for dt in data:
        if dt.startswith("node_filesystem_size_bytes"):
            key = dt.split(' ')[0].split('{')[1][:-1]
            value = int(float(dt.split(' ')[1]))/1024/1024
            if key in result.keys():
                result[key].update({"Total": value})
            else:
                result[key] = {"Total": value}
        elif dt.startswith("node_filesystem_avail_bytes"):
            key = dt.split(' ')[0].split('{')[1][:-1]
            value = int(float(dt.split(' ')[1]))/1024/1024
            if key in result.keys():
                result[key].update({"Available": value})
            else:
                result[key] = {"Available": value}
    return result
