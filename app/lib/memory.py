def memory():
    with open("/proc/meminfo") as memomy_file:
        lines = memomy_file.readlines()
        for line in lines:
            if line.split()[0] == 'MemTotal:':
                total = line.split()[1]
            elif line.split()[0] == 'MemAvailable:':
                available = line.split()[1]
    return total, available

